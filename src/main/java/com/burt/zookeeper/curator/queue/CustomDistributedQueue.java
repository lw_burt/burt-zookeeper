package com.burt.zookeeper.curator.queue;

/**
 * Description:消息队列接口
 * User: Burt
 * Date: 2017-08-22
 * Time: 11:11
 */
public interface CustomDistributedQueue<T> {

    /**
     *向队列提供数据
     * @param element
     * @return
     */
    boolean offer(T element);

    /**
     * 从队列中获取数据
     * @return
     */
    T poll();
}
