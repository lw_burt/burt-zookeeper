package com.burt.zookeeper.curator.queue;

import java.io.Serializable;

/**
 * Description: 存入队列中的消息
 * User: Burt
 * Date: 2017-08-22
 * Time: 9:29
 */
public class QueueMsg implements Serializable {

    private static final long serialVersionUID = -3711759221878152207L;

    /**
     * 消息id
     */
    private String id;

    /**
     * 消息内容
     */
    private String content;

    public QueueMsg(String id, String content) {
        this.id = id;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "QueueMsg{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
