package com.burt.zookeeper.curator.queue;

import java.util.concurrent.TimeUnit;

/**
 * Description:生产者
 * User: Burt
 * Date: 2017-08-22
 * Time: 11:08
 */
public class Producer implements Runnable  {
    private CustomDistributedQueue queue;

    public Producer(CustomDistributedQueue<QueueMsg> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 20; i ++){
                QueueMsg msg = new QueueMsg(String.valueOf(i), "content"+i);
                queue.offer(msg);
                System.out.println("成功发送一条消息: " + msg);
                TimeUnit.SECONDS.sleep((long) (Math.random() * 5));
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
