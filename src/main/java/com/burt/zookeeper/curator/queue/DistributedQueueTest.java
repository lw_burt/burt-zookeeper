package com.burt.zookeeper.curator.queue;

import com.burt.zookeeper.curator.CuratorClientUtils;
import com.burt.zookeeper.curator.queue.fifo.DistributedFifoQueue;
import com.burt.zookeeper.curator.queue.sync.DistributedSyncQueue;
import org.apache.curator.framework.CuratorFramework;

/**
 * Description: 分布式队列测试
 * User: Burt
 * Date: 2017-08-22
 * Time: 9:26
 */
public class DistributedQueueTest {

    public static void main(String[] args) {
        String root = "/queue";
        CuratorFramework curatorFramework = CuratorClientUtils.getInstance();
        //先进先出队列
//        DistributedFifoQueue<QueueMsg> queue = new DistributedFifoQueue<>(curatorFramework, root);

        //同步队列
        DistributedSyncQueue<QueueMsg> queue = new DistributedSyncQueue<>(curatorFramework, root,5);

        new Thread(new Producer(queue)).start();
        new Thread(new Consumer(queue)).start();
    }

}
