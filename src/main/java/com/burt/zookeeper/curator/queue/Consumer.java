package com.burt.zookeeper.curator.queue;

import java.util.concurrent.TimeUnit;

/**
 * Description: 消费者
 * User: Burt
 * ate: 2017-08-22
 * Time: 11:07
 */
public class Consumer implements Runnable {
    private CustomDistributedQueue queue;

    public Consumer(CustomDistributedQueue<QueueMsg> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 20; i++){
                TimeUnit.SECONDS.sleep((long) (Math.random() * 5));
                QueueMsg msg = (QueueMsg) queue.poll();
                if (null == msg){
                    continue;
                }
                System.out.println("成功消费一条消息: " + msg);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
