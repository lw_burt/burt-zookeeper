package com.burt.zookeeper.curator;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * Description: User: Burt
 * Date: 2017-08-08 Time: 23:03
 */
public class CuratorClientUtils {

    private final static String CONNECTSTRING="192.168.0.8:2181,192.168.0.7:2181," +
            "192.168.0.9:2181";

    private static CuratorFramework curatorFramework;

    public static CuratorFramework getInstance(){
        CuratorFramework curatorFramework = CuratorFrameworkFactory.builder().connectString(CONNECTSTRING).sessionTimeoutMs(5000).connectionTimeoutMs(5000)
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                .build();

        curatorFramework.start();

        return curatorFramework;
    }

}
