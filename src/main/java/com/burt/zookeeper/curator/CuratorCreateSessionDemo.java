package com.burt.zookeeper.curator;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * Description: User: Burt
 * Date: 2017-08-08 Time: 22:33
 */
public class CuratorCreateSessionDemo {
    private final static String CONNECTSTRING="192.168.0.8:2181,192.168.0.7:2181," +
            "192.168.0.9:2181";

    public static void main(String[] args) {
        //创建会话的两种方式
        CuratorFramework curatorFramework;
        curatorFramework = CuratorFrameworkFactory.newClient(CONNECTSTRING,5000,5000,
                new ExponentialBackoffRetry(1000,3));

        //start方式启动
        curatorFramework.start();

        //fluent风格
        CuratorFramework curatorFramework1 = CuratorFrameworkFactory.builder().connectString(CONNECTSTRING).sessionTimeoutMs(5000).connectionTimeoutMs(5000)
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                .namespace("curator").build();

        curatorFramework1.start();

        System.out.println("success");

    }
}
