package com.burt.zookeeper.curator;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.framework.api.transaction.CuratorTransactionResult;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;

/**
 * Description: User: Burt
 * Date: 2017-08-08 Time: 22:33
 */
public class CuratorOperatorDemo {

    public static void main(String[] args) {
        CuratorFramework curatorFramework = CuratorClientUtils.getInstance();
        System.out.println("连接成功....");

        /**
         * 创建节点
         */
//        try {
//            String result = curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
//                    .forPath("/curator/curator1/curator11", "123".getBytes());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        /**
         * 删除节点
         */
//        try {
//            //默认情况下，version为-1
//            //递归删除
//            curatorFramework.delete().deletingChildrenIfNeeded().forPath("/node");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        /**
         * 查询
         */
//        Stat stat = new Stat();
//        try {
//            byte[] bytes = curatorFramework.getData().storingStatIn(stat).forPath("/curator");
//            System.out.println(new String(bytes) + "-->stat: " + stat);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        /**
         * 更新
         */
//        try {
//            Stat stat = curatorFramework.setData().forPath("/curator", "123".getBytes());
//            System.out.println(stat);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        /**
         * 异步操作
         */
//        ExecutorService service = Executors.newFixedThreadPool(1);
//        //计数器
//        final CountDownLatch countDownLatch = new CountDownLatch(1);
//        try {
//            String s = curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL)
//                    .inBackground(new BackgroundCallback() {
//                        public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
//                            System.out.println(Thread.currentThread().getName() + "->reslutCode: "
//                                    + curatorEvent.getResultCode() +"->" + curatorEvent.getType());
//                          //递减
//                          countDownLatch.countDown();
//                        }
//                    }).forPath("/Burt", "123".getBytes());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        try {
//            //当countDownLatch的值为0时才会放行，否则等待
//            countDownLatch.await();
//            service.shutdown();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        /**
         * 事务操作
         */
        try {
            Collection<CuratorTransactionResult> collection = curatorFramework.inTransaction().create()
                    .forPath("/trans", "1254".getBytes())
                    .and().setData().forPath("/zzz", "666".getBytes()).and().commit();
            for (CuratorTransactionResult result : collection){
                System.out.println(result.getForPath() + "->"+result.getType());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
