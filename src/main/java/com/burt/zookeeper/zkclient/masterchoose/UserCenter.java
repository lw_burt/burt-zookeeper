package com.burt.zookeeper.zkclient.masterchoose;

import java.io.Serializable;

/**
 * Description: User: Burt Date: 2017-08-14 Time: 9:44
 */
public class UserCenter implements Serializable {

    private static final long serialVersionUID = -1776114173856665665L;

    /**
     * 机器信息
     */
    private Integer mcId;

    /**
     * 机器名称
     */
    private String mcName;

    public Integer getMcId() {
        return mcId;
    }

    public void setMcId(Integer mcId) {
        this.mcId = mcId;
    }

    public String getMcName() {
        return mcName;
    }

    public void setMcName(String mcName) {
        this.mcName = mcName;
    }

    @Override
    public String toString() {
        return "UserCenter{"
                + "mcId=" + mcId +
                ", mcName='" + mcName + '\'' +
                '}';
    }
}
