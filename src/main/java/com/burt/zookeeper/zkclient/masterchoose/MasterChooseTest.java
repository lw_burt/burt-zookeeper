package com.burt.zookeeper.zkclient.masterchoose;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;

/**
 * Description:master选举测试类
 * User: Burt
 * Date: 2017-08-14 Time: 15:50
 */
public class MasterChooseTest {

    private final static String CONNECTSTRING = "192.168.0.8:2181,192.168.0.7:2181," +
            "192.168.0.9:2181";

    private static final  int sessionTimeOut = 5000;
    private static final  int connectionTimeOut = 5000;

    public static void main(String[] args) {
        List<MasterSelector> selectorList = new ArrayList<>();
        ZkClient zkClient = null;
        UserCenter userCenter = null;
        MasterSelector masterSelector = null;
        try {
            for (int i = 0; i < 10; i++){
                zkClient = new ZkClient(CONNECTSTRING,sessionTimeOut,connectionTimeOut,
                        new SerializableSerializer());

                userCenter = new UserCenter();
                userCenter.setMcId(i);
                userCenter.setMcName("客户端 -> " + i);

                masterSelector = new MasterSelector(zkClient,userCenter);
                selectorList.add(masterSelector);
                //开始选举
                masterSelector.start();

                TimeUnit.SECONDS.sleep(1);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            for (MasterSelector selector :selectorList) {
                selector.stop();
            }
        }
    }

}
